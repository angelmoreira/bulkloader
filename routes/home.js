var express = require('express');
var router = express.Router();

const hd = require('../Lib/Handler');
const Handler = new hd();
var Utils = require('../Lib/Utils');

module.exports = router;

router.get('/', async (req, res) =>{

    await Handler.getBlobnamesAndFormatHtml()
        .then(result => Utils.successResHTML(result, res))
        .catch(error => Utils.errorResFx(error, res));

});

router.post('/send', async (req, res) => {

    await Handler.getBlobAndSend(req.body.blobname)
        .then(result => Utils.successResFx(result, res))
        .catch(error => Utils.errorResFx(error, res));

});
