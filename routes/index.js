var express = require('express');
var router = express.Router();

//Point to where we have our endpoint configuration
router.use('/', require('./home'));

module.exports = router;