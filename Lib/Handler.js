const az = require('../Lib/Azure');
const ff = require('./FileFunctions');
const Utils = require('./Utils');
var FileFunctions, Azure;

function Handler(){
    Azure = new az();
    FileFunctions = new ff();
}

Handler.prototype.getBlobAndSend = async function(blobname){
    return new Promise(async (resolve, reject) => {
    
        await Azure.getBlob(blobname)
        .then(async readableStreamBody => {
            await FileFunctions.streamToQueue(readableStreamBody)
                    .then(result => {
                        resolve(result);
                    })
                    .catch(error => Utils.errorRejFx(error, reject));
        })
        .catch(error => Utils.errorRejFx(error, reject));
    });
}

Handler.prototype.getBlobnamesAndFormatHtml = async function(){
    return new Promise(async (resolve, reject) =>{
        await Azure.getBlobNames()
            .then(blobArr => {
                var htmlRes =  FileFunctions.buildHtmlForm(blobArr);
                resolve(htmlRes);
            })
            .catch(error => Utils.errorRejFx(error, reject));
    });
}

module.exports = Handler;
