var blobServiceClient, queueServiceClient;
var Utils = require('./Utils');

//TODO extract all to env vars to use keyvault
const ACCOUNT = process.env.ACCOUNT_NAME;

const ACCOUNT_KEY = process.env.ACCOUNT_KEY;
const CONTAINER_NAME = process.env.CONTAINER_NAME;
const QUEUE_NAME = 'raw-message2'//`${process.env.QUEUE_NAME}`;
var queueClient,createQueueResponse;


function startBlobService(){
    var {BlobServiceClient, StorageSharedKeyCredential}= require("@azure/storage-blob");
    const sharedKeyCredential = new StorageSharedKeyCredential(ACCOUNT, ACCOUNT_KEY);
    blobServiceClient = new BlobServiceClient(
        `https://synchronybank.blob.core.windows.net/`,
        sharedKeyCredential
    );
}
function startQueueService(){
    var {QueueServiceClient, StorageSharedKeyCredential} = require("@azure/storage-queue");
    const sharedKeyCredential = new StorageSharedKeyCredential(ACCOUNT, ACCOUNT_KEY);
    queueServiceClient = new QueueServiceClient(
        `https://synchronybank.queue.core.windows.net/`,
        sharedKeyCredential,
        {
          retryOptions: { maxTries: 4 }, // Retry options
          telemetry: { value: "BasicSample/V11.0.0" } // Customized telemetry string
        }
    );
    queueClient = queueServiceClient.getQueueClient(QUEUE_NAME);
    createQueueResponse = queueClient.create();
}

function AzureFunctions() {
    startBlobService();
    startQueueService();
}


AzureFunctions.prototype.sendToQueue = function(message){
    return new Promise(async (resolve, reject) => {
        var encodedMsg = await Utils.EncodeBase64(JSON.stringify(message));
        resolve(await queueClient.sendMessage(encodedMsg));
    });    
}

AzureFunctions.prototype.getBlob = async function(blobname){

    return new Promise(async (resolve, reject) => {
        if(blobname){
            const containerClient = blobServiceClient.getContainerClient(CONTAINER_NAME);
            const blobClient = containerClient.getBlobClient(blobname);

            const downloadBlockBlobResponse = await blobClient.download().catch((error) => Utils.errorRejFx(error, reject));

            if(downloadBlockBlobResponse){
                resolve(downloadBlockBlobResponse.readableStreamBody);
            }
            else{
                reject(new Error("Incorrect blobname: " + blobname));
            }
        }
        else{
            reject(new Error("No name!"));
        }
    });
} 

AzureFunctions.prototype.getBlobNames = async function(){
    return new Promise(async (resolve,reject) => {
        var blobNames = [];
        containerClient = blobServiceClient.getContainerClient(CONTAINER_NAME);
        let i = 1;
        let iter = await containerClient.listBlobsFlat();
        for await (const blob of iter) {
            blobNames.push(blob.name);
        }
        
        resolve(blobNames);
    });
};

module.exports = AzureFunctions;