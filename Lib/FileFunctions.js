const csv = require('fast-csv');
const az = require('./Azure');
var Utils = require('../Lib/Utils');
var Helper = require('../Lib/Helper');
var Azure;
const beforeString = '<form action="send" method="post"><select name="blobname">';
const afterString = '</select><br><hr><input type="submit" value="Upload"></form>';

function FileFunctions() {
    Azure = new az();
}    

FileFunctions.prototype.streamToQueue = function(blobStream){
    
    return new Promise(async (resolve, reject) => {
       // var threeAgo = await Utils.ThreeMonthsAgo();
        var csvStream = csv
        .parseStream(blobStream, {headers: true})
        .on("data", async function(csvData){
            
            await Azure.sendToQueue(csvData)
                .then(result => {
                    console.log("Message sent to q ");
                })
                .catch(error => {
                    reject("Error sending to q ", error);
                });

        })
        .on("end", function(rowCount){
            resolve("Done reading CSV! " + rowCount + " " + new Date().toTimeString());
        })
        .on("error", error => Utils.errorRejFx(error, reject))
    });
};

FileFunctions.prototype.buildHtmlForm = async function(blobArr){

    var outputStr =[];
    
    outputStr.push(beforeString);
    await blobArr.forEach(blobName => {   
        var tempStr = `<option value="${blobName}">${blobName}</option>`
        outputStr.push(tempStr);
    });
    outputStr.push(afterString);
    outputStr.join("");
    
    return outputStr.toString();
}

module.exports = FileFunctions;