// WEBSITE_MAX_DYNAMIC_APPLICATION_SCALE_OUT
var queueClient;
module.exports = {
    EncodeBase64: async function(message){
        return new Promise ((resolve, reject) => {
            
            if(!message) reject("No message ", message)
            let binaryData = Buffer.from(message, "utf8");
            resolve(binaryData.toString("base64"));
        });
    },
    ThreeMonthsAgo: async function(){
        return new Promise((resolve, reject) => {
            var now = new Date();
            resolve(new Date(now.getFullYear(), now.getMonth()-3, now.getDate() ));
        });
    },
    Log: async function(msg){
        queueClient = await queueServiceClient.getQueueClient(LOGGING[msg.queueName]);
        createQueueResponse = await queueClient.create();

        var encodedMsg = await this.EncodeBase64(msg.message);
        await queueClient.sendMessage(encodedMsg);
    },
    errorRejFx: async function(error, reject){
        console.log("ERROR fx!" );
        console.log(error);
        this.Log({
            "message": error,
            "queueName": 'error'
            });
        reject(error);
    },
    errorRetFx: async function(error){
        console.log("ERROR fx!" );
        console.log(error);
        this.Log({
            "message": error,
            "queueName": 'error'
            });
        return(null);
    },
    errorResFx: async function(error, context){
        console.log("ERROR fx!" );
        console.log(error);
        this.Log({
            "message": error,
            "queueName": 'error'
            });
        context.res = {
            status: 407,
            body: error
        };
    },
    successResFx: async function(result, res){
        console.log("SUCCESS fx!" );
        console.log(result);
        this.Log({
            "message": result,
            "queueName": 'success'
            });
        res.send({
            status: 201,
            body: result
        });
    },
    successResolveFx: async function(result, resolve){
        console.log("SUCCESS! ", result);
        resolve(result);
    },
    successResHTML: async function(result, res){
        res.append('Content-Type', 'text/html');
        res.send(result);
    }

}

startQueueService();
function startQueueService(){
    var {QueueServiceClient, StorageSharedKeyCredential} = require("@azure/storage-queue");
    const sharedKeyCredential = new StorageSharedKeyCredential(process.env.ACCOUNT_NAME, process.env.ACCOUNT_KEY);
    queueServiceClient = new QueueServiceClient(
        `https://synchronybank.queue.core.windows.net/`,
        sharedKeyCredential,
        {
          retryOptions: { maxTries: 4 }, // Retry options
          telemetry: { value: "BasicSample/V11.0.0" } // Customized telemetry string
        }
    );
}

const LOGGING = {
    "error": "error-queue",
    "success": "saved-tids",
    "blob": "blob-success"
}



