//JS Helper for Synchrony POC

module.exports = {
    getDate: async function(dataObj){
        var eventDate;
        //JOINED DATASET && COMPLAINTS
        if(dataObj.received_date){
            eventDate = await this.fixDate(new Date(dataObj.received_date.substring(0,9)));
        }
        //FLOOR
        else if(dataObj.PAGE_LOAD_DATE){
            eventDate = await this.fixDate(new Date(dataObj.PAGE_LOAD_DATE.substring(0,9)));
        }
        //IVR
        else if(dataObj.CALL_END_TIME){
            eventDate = await this.fixDate(new Date(dataObj.CALL_END_TIME.substring(0,9)));
        }
        //ACCOUNTBASE
        else if(dataObj.OPEN_DATE){
            eventDate = await this.fixDate(new Date(dataObj.OPEN_DATE.substring(0,9)));
        }
        //TRANSACTION
        else if(dataObj.TRANSACTION_DATE){
            eventDate = await this.fixDate(new Date(dataObj.TRANSACTION_DATE.substring(0,9)));
        }
        //ECOM 
        else if(dataObj.DATE_TIME){
            eventDate = await this.fixDate(new Date(dataObj.DATE_TIME.substring(0,9)));
        }

        return eventDate;
    },   
    fixDate: async function(date){
        var now = new Date();
        var newDate = new Date(date.getFullYear(), date.getMonth()+6, date.getDate());
        return(newDate > now ? now : newDate);
    },
    getActivityType: async function(data, propCode){
            var rawString = data.CCRPID;
            var activitiesArr = [];
            var touchpoint, dynPropCode, acTypeCode, timestamp;
            var keys = Object.keys(activitiesObj);

            keys.forEach(key => {
                if((rawString) && rawString.includes(key)){
                    if(!activitiesArr[0] || activitiesArr[0].activityTypeCode !== 'make_payment'){
                        touchpoint = activitiesObj[key].touchpoint;
                        dynPropCode = propCode.dynamicPropositionCode;
                        acTypeCode = activitiesObj[key].activityTypeCode;
                        timestamp = data.timestamp;
                    }
                }
                else if(data.UNIQUE_CALL_KEY && (data[key] === '1')){
                    touchpoint = activitiesObj[key].touchpoint;
                    dynPropCode = propCode.dynamicPropositionCode;
                    acTypeCode = activitiesObj[key].activityTypeCode;
                    timestamp = data.timestamp;
                }
                else if(data.TRANSACTION_DATE && data.TRANSACTION_CODE === '253'){
                    touchpoint = activitiesObj[key].touchpoint;
                    dynPropCode = propCode.dynamicPropositionCode;
                    acTypeCode = activitiesObj[key].activityTypeCode;
                    timestamp = data.timestamp;
                }
                else if(data.DATE_TIME && (data.HIGH_LEVEL_FUNCTION === key)){
                    if( (key === 'activity and payments' && data.LOW_LEVEL_FUNCTION === 'make a payment') ||
                        (key === 'card activation') ){
                        
                        touchpoint = activitiesObj[key].touchpoint;
                        dynPropCode = propCode.dynamicPropositionCode;
                        acTypeCode = activitiesObj[key].activityTypeCode;
                        timestamp = data.timestamp;
                    }
                }
                else if(data.OPEN_DATE && data[key]){
                    touchpoint = activitiesObj[key].touchpoint;
                    dynPropCode = propCode.dynamicPropositionCode;
                    acTypeCode = activitiesObj[key].activityTypeCode;
                    timestamp = data.timestamp;
                }
                else if(data[key]){
                    touchpoint = activitiesObj[key].touchpoint;
                    dynPropCode = propCode.dynamicPropositionCode;
                    acTypeCode = activitiesObj[key].activityTypeCode;
                    timestamp = data.timestamp;
                }

                if(acTypeCode){
                    if(dynPropCode){
                        activitiesArr.push({
                            "propositionCode": "partners",
                            "dynamicPropositionCode": dynPropCode,
                            "activityTypeCode": acTypeCode,
                            "timestamp": timestamp
                        });
                    }
                    else{
                        activitiesArr.push({
                            "propositionCode": propCode,
                            "activityTypeCode": acTypeCode,
                            "timestamp": timestamp
                        });
                    }
                    acTypeCode = null;dynPropCode = null;
                }
            });
            if(activitiesArr[0]){
               return({'url': touchpoint, 'activities': activitiesArr});
            }
            else{
                console.log("NO activities! dont send");
                return null;
            }

    },
    formatActivities: async function(data){
        var propCode = await this.getPropCode((data.CLIENT_ID || data.client));
        
        data.activitiesObj = await this.getActivityType(data, propCode);
        
        if(data.activitiesObj){
            return await this.formatBody(data);
        }
        else{
            return null;
        }
    },
    getPropCode: async function(clientId){
        return propsObj[clientId] ? propsObj[clientId] : {"dynamicPropositionCode": clientId};
    },
    formatBody: async function(dataObj){
        var responseBody = {};
        responseBody.customerContext = {
            'identifiers': [
                {
                    "apiName": 'customerkey',
                    "value": dataObj.token_id
                }
            ],
            "baseTouchpointUri": dataObj.activitiesObj.url || "assist://callcenter/"
        }
        responseBody.activities = dataObj.activitiesObj.activities;
        return responseBody;
    }
}

var activitiesObj = { 
    "Card Activation": {
      "touchpoint": "assist://callcenter/",
      "activityTypeCode": "activate_card"      
    },
    "Payment processed": {
        "touchpoint": "assist://callcenter/",
        "activityTypeCode": "make_payment"
      },
    "Complaint-Cust Service Use Only": {
      "touchpoint": "assist://callcenter/",
      "activityTypeCode": "make_payment"
    },
    "ACH Payment": {
      "touchpoint": "assist://callcenter/",
      "activityTypeCode": "make_payment"
    },
    "Active_Acct": {
      "touchpoint": "assist://ivr/",
      "activityTypeCode": "activate_card" 
    },
    "Pay_Today":{
        "touchpoint": "assist://ivr/",
        "activityTypeCode": "make_payment" 
    },
    "Request to Pay By Phone": { 
      "touchpoint": "assist://ivr/",
      "activityTypeCode": "make_payment"
    },
    // "Resolved": {
    //   "activityTypeCode": "resolved",
    // },
    "activated": {
        "activityTypeCode": "get_card",
        "touchpoint": "phys://account/"
    },
    "card activation": {
        "activityTypeCode": "activate_card",
        "touchpoint": "https://www.mysynchrony.com/"
    },
    "activity and payments": {
        "activityTypeCode": "make_payment",
        "touchpoint": "https://www.mysynchrony.com/"
    },
    "complaint_type": {
        "touchpoint": "assist://callcenter/",
        "activityTypeCode": "file_complaint"
    }
}

var propsObj = {
  "LOWES": "lowes",
  "PAYPALCR":	"paypalcr",
  "PAYPALDUAL": "paypaldual",
  "SAMSCNDSVR":	"samscndsvr",
  "AMAZON":	"amazoncon",
  "SAMSCONS":	"samscons",
  "TJXDC": "tjxdc"
}
