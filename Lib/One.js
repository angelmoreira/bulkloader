var rp = require('request-promise');

const PROTOCOL = "https://";
const TOKEN_URI = "/one/oauth2token";
const HTTP_VERB_POST = "POST";
const CONTENT_TYPE_URL_ENCODED = "application/x-www-form-urlencoded";
const CONTENT_TYPE_JSON = "application/json";
const GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";
const V3_API_URI = "/one/oauth2/rt/api/3.1.0/customer/";
var Utils = require('./Utils');

var token;

function HandleActivity(){}

HandleActivity.prototype.refreshToken = async function(){
    return new Promise(async (resolve, reject) => {
        
        var now = new Date().getTime();
    
        if(!token || (token && now >= token.expiresOn)){
            var requestOptions = {
                uri: PROTOCOL + (process.env.HOSTNAME ) + TOKEN_URI,
                method: HTTP_VERB_POST,
                headers: {
                    "Authorization":  
                        ("Basic " + new Buffer.from(process.env.CLIENT_ID + ":" + process.env.CLIENT_SECRET, "utf8").toString("base64")),
                    "Content-Type": CONTENT_TYPE_URL_ENCODED
                },
                form: {grant_type: GRANT_TYPE_CLIENT_CREDENTIALS}
            };
            
            await rp(requestOptions)
                    .then(body => {
                        token = JSON.parse(body);
                        token.expiresOn = (new Date().getTime()) + ((token.expires_in * 1000) - 5000);
                        resolve(token);
                    })
                    .catch(error => Utils.errorRejFx(error, reject));
        }
        else {
            resolve(token);
        }
    });
    
};

HandleActivity.prototype.sendRequest = async function(data){
    
    return new Promise(async (resolve, reject) => {
        await this.refreshToken()
            .then(async tokenObject => {
                
                if(tokenObject){            
                    var requestOptions = {
                        uri: PROTOCOL + process.env.HOSTNAME + V3_API_URI + process.env.SITE_KEY + "/activity",
                        method: HTTP_VERB_POST,
                        headers: {
                            'Accept': CONTENT_TYPE_JSON,
                            'Content-Type': CONTENT_TYPE_JSON,
                            'Authorization': "Bearer " + token.access_token
                        },
                        body: JSON.stringify(data)
                    };

                    await rp(requestOptions)
                            .then(body => Utils.successResolveFx(body,resolve))
                            .catch(error => Utils.errorRejFx(error, reject));
                }
        });      
    });
    
};

module.exports = HandleActivity;
